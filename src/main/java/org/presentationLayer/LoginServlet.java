package org.presentationLayer;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.bussinesLayer.FlightService;
import org.bussinesLayer.UserService;
import org.entities.City;
import org.entities.Flight;

public class LoginServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private UserService userService;
	private FlightService flightService;
	
	public void init(){
		userService = new UserService();
		flightService = new FlightService();
		
	}
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("role") != null)
			session.removeAttribute("role");
		
		request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
		
	}
	
	public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String username = (String) request.getParameter("username");
		String password  = (String) request.getParameter("password");
		String url = null;
		if(userService.verifyUser(username, password)){
			String role = userService.getUserRole(username);
			
				session.setAttribute("role", role);
		
			ArrayList< Flight> flights = (ArrayList<Flight>) flightService.getAllFlight();
			request.setAttribute("flights", flights);
			
			if(role.equals("Admin")){
				ArrayList<City> cities = (ArrayList<City>) flightService.getAllCities();
				request.setAttribute("cities", cities);
				url = "/WEB-INF/admin.jsp";
			 }
			 else url = "/WEB-INF/user.jsp";
			
			
		}
		else{
			url = "/WEB-INF/login.jsp";
			request.setAttribute("error", "Faild login");
		}
			
	
		request.getRequestDispatcher(url).forward(request, response);
		
	}
	
}
