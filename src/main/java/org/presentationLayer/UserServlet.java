package org.presentationLayer;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.bussinesLayer.FlightService;
import org.entities.Flight;
import org.jdom2.Element;
import org.jdom2.input.DOMBuilder;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class UserServlet extends HttpServlet{

/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	FlightService flightService ;
	
	public void init(){
		
		flightService = new FlightService();
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		//String username = (String) session.getAttribute("userId");
		String role = (String) session.getAttribute("role");
		String url = null;
		if(role == null){
			request.setAttribute("error", "You don't have the authority to access that page");
			 url = "/WEB-INF/login.jsp";
			  }
		else{
			try{
				
				ArrayList< Flight> flights = (ArrayList<Flight>) flightService.getAllFlight();
				request.setAttribute("flights", flights);
				
				}
				catch(Exception e){
					System.out.println("Error"+e);
				
				}
			url = "/WEB-INF/user.jsp";
		}
		request.getRequestDispatcher(url).forward(request, response);
	}
	
	public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
	HttpSession session = request.getSession();
		
		//String username = (String) session.getAttribute("userId");
		String role = (String) session.getAttribute("role");
		String url = null;
		if(role == null){
			request.setAttribute("error", "You don't have the authority to access that page");
			 url = "/WEB-INF/login.jsp";
			  }
		else{
			
			try{
				
				String departureCityString = (String) request.getParameter("departure");
				String arrivalCityString = (String) request.getParameter("arrival");
				
				ArrayList<Double> departureCoord = flightService.cityCoordinates(departureCityString);
				ArrayList<Double>  arrivaleCoord = flightService.cityCoordinates(arrivalCityString);
				String url1 = "http://new.earthtools.org/timezone";
				String finalUrl = url1;
				for(Double d: departureCoord)
					finalUrl += "/" + d;
				System.out.println(finalUrl);
				URLConnection connection = new URL(finalUrl).openConnection();
				InputStream responseTime = connection.getInputStream();
				
				
				request.setAttribute("departureCityTime",readResponse(responseTime));
				responseTime.close();
				Thread.sleep(2000);
				finalUrl = url1;
				for(Double d: arrivaleCoord)
					finalUrl += "/" + d;
				System.out.println(finalUrl);
				URLConnection connection1 = new URL(finalUrl).openConnection();
					
				InputStream responseTime1 = connection1.getInputStream();
				request.setAttribute("arrivalCityTime",readResponse(responseTime1));
				responseTime1.close();
				
	
			
			}
			catch(Exception e){
				System.out.println("Error"+e);
			
			}
			
			ArrayList< Flight> flights = (ArrayList<Flight>) flightService.getAllFlight();
			request.setAttribute("flights", flights);
			
			url = "/WEB-INF/user.jsp";
		}
		request.getRequestDispatcher(url).forward(request, response);
	}

	private String readResponse(InputStream responseTime) {
		
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document xmlDoc = db.parse(responseTime);
			DOMBuilder ddb = new DOMBuilder();
			org.jdom2.Document jdomDoc = ddb.build(xmlDoc); 
			Element root = jdomDoc.getRootElement();
			
			/*Element el = root.getChild("utctime");
			System.out.println(el.getText());
			
			el = root.getChild("isotime");
			System.out.println(el.getText());*/
			
			Element el = root.getChild("localtime");
			//System.out.println(el.getText());
			
			return el.getText();
		} catch (ParserConfigurationException | SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
		
	}
}
