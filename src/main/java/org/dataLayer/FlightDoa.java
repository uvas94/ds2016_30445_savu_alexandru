package org.dataLayer;

import java.util.List;


import org.entities.Flight;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class FlightDoa extends AbstractDao<Integer, Flight>{
	
	public Flight findFlightById(int id) {
		return getByKey(id);
	}

	
	public boolean saveFlight(Flight flight) {
			return persist(flight);
		
	}

	
	@SuppressWarnings("rawtypes")
	public boolean deleteFlightById(int id) {
		Session session = getSession();
		Transaction tx = null;
		
		try{
		tx = session.beginTransaction();
			
			
		Query query = session.createQuery("DELETE FROM Flight WHERE flightid=:id");
        query.setParameter("id", id);
        query.executeUpdate();
        tx.commit();
		}
		catch(Exception e){
			System.out.println("Error"+e);
			if (tx != null) {
				tx.rollback();
			}
			
			return false;
		} finally {
			session.close();
		}
		return true;
	}

	
	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<Flight> findAllFlights() {
		
  
        Session session = getSession();
		Transaction tx = null;
		List<Flight> flights = null;
		try {
			tx = session.beginTransaction();
			flights = session.createQuery("FROM Flight").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return flights;
	}
	
}
