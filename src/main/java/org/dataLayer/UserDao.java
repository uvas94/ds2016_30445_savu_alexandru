package org.dataLayer;

import java.util.List;


import org.entities.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class UserDao extends AbstractDao<Integer,User>{
	
	public User findUserById(int id) {
		return getByKey(id);
	}

	
	public void saveUser(User user) {
			persist(user);
	}

	
	@SuppressWarnings("rawtypes")
	public void deleteUserById(int id) {
		Query query = getSession().createQuery("delete from User where iduser =:id");
        query.setParameter("id", id);
        query.executeUpdate();
	}


	
	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<User> findAllUsers() {
		
  
        Session session = getSession();
		Transaction tx = null;
		List<User> users = null;
		try {
			tx = session.beginTransaction();
			users = session.createQuery("FROM User").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return users;
	}
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<User> findUserByName(String name) {
	     Session session = getSession();
			Transaction tx = null;
			List<User> users = null;
			try {
				tx = session.beginTransaction();
				Query<User> query =  session.createQuery("FROM User WHERE username =:name");
				query.setParameter("name", name);
				users = query.list();
				tx.commit();
			} catch (HibernateException e) {
				if (tx != null) {
					tx.rollback();
				}
			} finally {
				session.close();
			}
			return users;
		
	}
	
	
}
