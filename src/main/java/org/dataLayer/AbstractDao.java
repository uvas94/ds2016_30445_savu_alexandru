package org.dataLayer;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public abstract class AbstractDao<PK extends Serializable, T> {
	
	private final Class<T> persistentClass;
	
	@SuppressWarnings("unchecked")
	public AbstractDao(){
        this.persistentClass =(Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }
     
	private static SessionFactory sessionFactory;
	
	public Session getSession(){
		return getSessionFactory().openSession();
	}

    public T getByKey(PK key) {
    	T obj = null;
    	Session s = getSession();
    	try{
    	obj  = s.get(persistentClass, key);
    	}
    	catch(Exception e){
    		
    		System.out.println("Get error: "+e);
    	}
    	finally{
    		s.close();
    	}
    	return obj;
    }
 
    public boolean persist(T entity) {
    	
    	Session s = getSession();
    	Transaction tx = null;
    	try{
    		tx = s.beginTransaction();
    		s.saveOrUpdate (entity);
    		tx.commit();
    	}
    	catch(Exception e){
    		
    		System.out.println("Save error: "+e);
    		if (tx != null) {
				tx.rollback();
				}
    		return  false;
    	}
    	finally{
    		s.close();
    	}
       
    	return true;
    }
 
    public boolean delete(T entity) {
    	Session s = getSession();
    	try{
    		s.delete(entity);;
    	}
    	catch(Exception e){
    		
    		System.out.println("Delete error: "+e);
    		return false;
    	}
    	finally{
    		s.close();
    	}
    	return true;
    }
	
    
    private SessionFactory getSessionFactory(){
    	if(sessionFactory == null)
    		sessionFactory = new Configuration().configure().buildSessionFactory();
    	
    	return sessionFactory;
 
    }
}
