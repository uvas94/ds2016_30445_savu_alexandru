package org.entities;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.persistence.Entity;
import javax.persistence.Table;



@Entity
@Table(name = "user")
public class User {
	
	@Id
	@Column(name  = "iduser")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int iduser;
	
	@NotNull
	@Column(name = "username")
	private String username;
	
	@Size (min = 6)
	@NotNull
	@Column(name = "password")
	private String password;
	
	@NotNull
	@Column(name = "role")
	private String role;
	
	
	
	public User() {
		super();
	}
	public User(int iduser, String username, String password, String role) {
		super();
		this.iduser = iduser;
		this.username = username;
		this.password = password;
		this.role = role;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
}
