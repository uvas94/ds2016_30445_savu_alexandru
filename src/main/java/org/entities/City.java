package org.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "city")
public class City {
	@Id
	@Column(name = "idcity")
	int id;
	
	@NotNull
	@Column(name  = "name")
	String cityName;
	
	@NotNull
	@Column(name = "long")
	double longitude;
	
	@NotNull
	@Column( name = "lat")
	double latitude;


	public City() {
		super();
	}


	public City(int id, String cityName, double longitude, double latitude) {
		super();
		this.id = id;
		this.cityName = cityName;
		this.longitude = longitude;
		this.latitude = latitude;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getCityName() {
		return cityName;
	}


	public void setCityName(String cityName) {
		this.cityName = cityName;
	}


	public double getLongitude() {
		return longitude;
	}


	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}


	public double getLatitude() {
		return latitude;
	}


	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	
}
