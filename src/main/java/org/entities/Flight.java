package org.entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "flight")
public class Flight {
	
	@Id
	@NotNull
	@Column(name = "flightid")
	int id;
	
	@Column(name = "airplanetype")
	String airplaneType;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "departurecity", nullable = false)
	City departureCity;
	
	@NotNull
	@Column(name = "departuredate")
	Date departureDate;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "arrivalcity", nullable = false)
	City arrivalCity;
	
	@NotNull
	@Column(name = "arrivaldate")
	Date arrivalDate;


	public Flight(){}
	
	public Flight(int id, String airplaneType, City departureCity,
			Date departureDate, City arrivalCity, Date arrivalDate) {
		super();
		this.id = id;
		this.airplaneType = airplaneType;
		this.departureCity = departureCity;
		this.departureDate = departureDate;
		this.arrivalCity = arrivalCity;
		this.arrivalDate = arrivalDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAirplaneType() {
		return airplaneType;
	}

	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}

	public City getDepartureCity() {
		return departureCity;
	}

	public void setDepartureCity(City departureCity) {
		this.departureCity = departureCity;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public City getArrivalCity() {
		return arrivalCity;
	}

	public void setArrivalCity(City arrivalCity) {
		this.arrivalCity = arrivalCity;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	
	
	
}
